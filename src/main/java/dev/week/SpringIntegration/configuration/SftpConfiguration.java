
package dev.week.SpringIntegration.configuration;

import dev.week.SpringIntegration.transformer.DecryptTransformer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.amqp.dsl.Amqp;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.sftp.dsl.Sftp;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;

import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
public class SftpConfiguration {

	@Bean
	public DefaultSftpSessionFactory createSftpSessionFactory() {
		DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory();
		factory.setHost("localhost");
		factory.setPort(22);
		factory.setAllowUnknownKeys(true);
		factory.setUser("nemanja");
		factory.setPassword("pass1234");

		return factory;
	}

	@Autowired
	DecryptTransformer decryptTransformer;

	@Bean
	public IntegrationFlow sftpIntegrationFlow(RabbitTemplate rabbitTemplate) {
		return IntegrationFlows.from(Sftp.inboundAdapter(createSftpSessionFactory())
						.remoteDirectory("sftp/upload")
						.regexFilter(".*\\.txt$")
						.autoCreateLocalDirectory(true)
						.temporaryFileSuffix(".writing")
//						.maxFetchSize(1)
						.localDirectory(new File("tmp/incoming")),
					e -> e.id("sftpInboundAdapter")
						.autoStartup(true)
						.poller(Pollers.fixedDelay(10000)
								.maxMessagesPerPoll(3)
								.errorChannel("sftpErrorChannel")))
				.channel(MessageChannels.executor(executor()))
				.handle(Amqp.outboundGateway(rabbitTemplate).routingKey("test1"))
//				.transform(File::getName)
				.<Integer, String>transform(Object::toString)
				.handle(message -> {
					System.out.println("Result from the handler: " + message.getPayload());
					System.out.println("Get class on our app: " + message.getPayload().getClass());
					System.out.println("Thread: " + Thread.currentThread().getName());
				})
				.get();
	}

	@Bean
	public IntegrationFlow rabbitAmqp(ConnectionFactory connectionFactory) {
		return IntegrationFlows.from(Amqp.inboundGateway(connectionFactory, "test1"))
				.transform( t -> {
					System.out.println("Get class on remote app: " + t.getClass());
					return fibonacciRecursion(40);
				})
				.get();
	}

	@Bean
	public IntegrationFlow sftpErrors() {
		return IntegrationFlows.from("sftpErrorChannel")
				.handle(message -> {
					System.out.println(" ************************************** Error: " + message.getPayload());
				})
				.get();
	}

	private Integer fibonacciRecursion(int n) {
		if(n == 0){
			return 0;
		}

		if(n == 1 || n == 2){
			return 1;
		}

		return fibonacciRecursion(n-2) + fibonacciRecursion(n-1);
	}

	@Bean
	public Executor executor() {
		return Executors.newFixedThreadPool(5);
	}
}

