
package dev.week.SpringIntegration.transformer;

import org.apache.commons.io.FileUtils;
import org.springframework.integration.file.transformer.AbstractFilePayloadTransformer;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Component
public class DecryptTransformer extends AbstractFilePayloadTransformer<File> {

	@Override
	protected File transformFile(File file) throws IOException {
		String s = FileUtils.readFileToString(file, "UTF-8");
		System.out.println(String.format("**** Transformer encrypted text: %s", s));
		s+= "Decryprted";
		File decryptedFile = new File("decryptedFile.txt");
		return decryptedFile;
	}
}
